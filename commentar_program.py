from secrets import token_bytes
from typing import Tuple

def random_key(length: int) -> int:
    # Dan en række bytes af en relativt tilfældig bytes i en givet længde.
    tb: bytes = token_bytes(length)
    # Konverter bytes'ne til en bit streng, og send det tilbage.
    return int.from_bytes(tb, "big")

def encrypt(original: str) -> Tuple[int, int]:
    original_bytes: bytes = original.encode()
    dummy: int = random_key(len(original_bytes))
    original_key: int = int.from_bytes(original_bytes, "big")
    # big betyder, at mest betydende bit er i starten (venstre), altså den bit, der repræsentere den højeste numeriske værdi
    encrypted: int = original_key ^ dummy  # XOR
    return dummy, encrypted

def decrypt(key1: int, key2: int) -> str:
    decrypted: int = key1 ^ key2  # XOR
    temp: bytes = decrypted.to_bytes((decrypted.bit_length() + 7) // 8, "big")
    return temp.decode()

if __name__ == "__main__":
    key1, key2 = encrypt("One Time Pad!")
    # Print nøglerne i rækkefølge.
    result: str = decrypt(key1, key2)
    print(result)
