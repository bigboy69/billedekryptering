# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class mainFrame
###########################################################################

class mainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		gSizer1 = wx.GridSizer( 0, 2, 0, 0 )
		
		
		self.SetSizer( gSizer1 )
		self.Layout()
		self.m_menubar1 = wx.MenuBar( 0 )
		self.file = wx.Menu()
		self.crypt = wx.MenuItem( self.file, wx.ID_ANY, u"Krypter", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.crypt )
		
		self.decrypt = wx.MenuItem( self.file, wx.ID_ANY, u"Dekrypter", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.decrypt )
		
		self.m_menubar1.Append( self.file, u"File" ) 
		
		self.SetMenuBar( self.m_menubar1 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_MENU, self.crypt_meth, id = self.crypt.GetId() )
		self.Bind( wx.EVT_MENU, self.decrypt_meth, id = self.decrypt.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def crypt_meth( self, event ):
		event.Skip()
	
	def decrypt_meth( self, event ):
		event.Skip()
	

###########################################################################
## Class cryptFrame
###########################################################################

class cryptFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer3 = wx.BoxSizer( wx.VERTICAL )
		
		self.filePick1 = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		bSizer3.Add( self.filePick1, 0, wx.ALL, 5 )
		
		self.crypt_btn = wx.Button( self, wx.ID_ANY, u"Krypter", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.crypt_btn, 0, wx.ALL, 5 )
		
		self.bitmapCrypt = wx.StaticBitmap( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.bitmapCrypt, 0, wx.ALL, 5 )
		
		
		self.SetSizer( bSizer3 )
		self.Layout()
		self.m_menubar2 = wx.MenuBar( 0 )
		self.file1 = wx.Menu()
		self.exit = wx.MenuItem( self.file1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.file1.AppendItem( self.exit )
		
		self.m_menubar2.Append( self.file1, u"File" ) 
		
		self.SetMenuBar( self.m_menubar2 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.crypt_btn.Bind( wx.EVT_BUTTON, self.crypt_method )
		self.Bind( wx.EVT_MENU, self.close, id = self.exit.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def crypt_method( self, event ):
		event.Skip()
	
	def close( self, event ):
		event.Skip()
	

###########################################################################
## Class decryptFrame
###########################################################################

class decryptFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.filePick2 = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		bSizer4.Add( self.filePick2, 0, wx.ALL, 5 )
		
		self.decrypt_btn = wx.Button( self, wx.ID_ANY, u"Dekrypter", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.decrypt_btn, 0, wx.ALL, 5 )
		
		self.bitmapDecrypt = wx.StaticBitmap( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.bitmapDecrypt, 0, wx.ALL, 5 )
		
		
		self.SetSizer( bSizer4 )
		self.Layout()
		self.m_menubar4 = wx.MenuBar( 0 )
		self.m_menu4 = wx.Menu()
		self.m_menuItem5 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu4.AppendItem( self.m_menuItem5 )
		
		self.m_menubar4.Append( self.m_menu4, u"File" ) 
		
		self.SetMenuBar( self.m_menubar4 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.decrypt_btn.Bind( wx.EVT_BUTTON, self.decrypt_method )
		self.Bind( wx.EVT_MENU, self.close1, id = self.m_menuItem5.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def decrypt_method( self, event ):
		event.Skip()
	
	def close1( self, event ):
		event.Skip()
	

