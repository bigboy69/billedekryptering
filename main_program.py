import wx
import GUI
from secrets import token_bytes
from typing import Tuple
import base64
import pickle
from wx.lib.pubsub import pub

def random_key(length: int) -> int:
    tb: bytes = token_bytes(length)
    return int.from_bytes(tb, "big")

class mainFrame(GUI.mainFrame):
    def __init__(self, parent):
        GUI.mainFrame.__init__(self, parent)
        self.cryptFrame = cryptFrame(self)
        self.decryptFrame = decryptFrame(self)

    def crypt_meth( self, event ):
        self.cryptFrame.Show()
        #self.key1, self.key2 = self.encrypt()

    def decrypt_meth( self, event ):
        self.decryptFrame.Show()
        #result: str = self.decrypt(self.key1, self.key2)

class cryptFrame(GUI.cryptFrame):
    def __init__(self, parent):
        GUI.cryptFrame.__init__(self,parent)

    def bitmapCrypt(self, event):
        pic = wx.Image(self.filePick1.GetPath(), wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.filePick1.SetBitmap(pic)

    def crypt_method( self, event ):
        filename = 'kryptpic'
        outfile = open(filename, 'wb')
        with open(self.filePick1.GetPath(), "rb") as imageFile:
            streng = base64.b64encode(imageFile.read())
            # Her skal billedet krypteres, og så skal det dumpes
            #original_bytes: bytes = streng.encode()
            dummy: int = random_key(len(streng))
            original_key: int = int.from_bytes(streng, "big")
            encrypted: int = original_key ^ dummy
            pickle.dump(int.from_bytes(streng, "big"), outfile)
            outfile.close()
            pub.sendMessage("keys", key1 = original_key)
            pub.sendMessage("keys2", key2 = dummy)
        self.Hide()


    def encrypt(original: str) -> Tuple[int, int]:
        original_bytes: bytes = original.encode()
        dummy: int = random_key(len(original_bytes))
        original_key: int = int.from_bytes(original_bytes, "big")
        encrypted: int = original_key ^ dummy
        return dummy, encrypted

    def close(self, event ):
        self.Hide()

class decryptFrame(GUI.decryptFrame):
    def __int__(self, parent):
        GUI.decryptFrame.__init__(self,parent)
        pub.subscribe(self.listner, "keys")
        pub.subscribe(self.listner2, "keys2")

    def listner(self, key1):
        self.encrypted_key = key1

    def listner2(self, key2):
        self.dummy = key2

    def decrypt_method( self, event ):
        infile = open(self.filePick2.GetPath(), 'rb')
        pic_som_int = pickle.load(infile)
        print(pic_som_int)
        infile.close()
        # Her skal der dekrypteres
        decrypted: int = self.dummy ^ self.encrypted_key
        temp: bytes = pic_som_int.to_bytes((decrypted.bit_length() + 7) // 8, "big")
        pic = base64.b64decode(temp)
        filename = 'new_image.jpg'
        with open(filename, 'wb') as f:
            f.write(pic)
        self.bitmapDecrypt.SetBitmap(wx.Image(filename, wx.BITMAP_TYPE_ANY).ConvertToBitmap())

    def decrypt(key1: int, key2: int) -> str:
        decrypted: int = key1 ^ key2
        temp: bytes = decrypted.to_bytes((decrypted.bit_length() + 7) // 8, "big")
        return temp.decode()

    def close1( self, event ):
        self.Hide()

app = wx.App(False)
frame = mainFrame(None)
frame.Show(True)
app.MainLoop()