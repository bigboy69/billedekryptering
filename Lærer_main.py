import wx
import gui
import base64
import pickle

class dekrypt(gui.Dekrypt):
    def __init__(self, parent):
        gui.Dekrypt.__init__(self, parent)

    def dekrypt_file( self, event ):
        infile = open(self.m_filePicker2.GetPath(), 'rb')
        pic_som_int = pickle.load(infile)
        infile.close()
        # Her skal der dekrypteres
        pic_som_bytes = pic_som_int.to_bytes((pic_som_int.bit_length() + 7) // 8, "big")
        pic = base64.b64decode(pic_som_bytes)
        filename = 'new_image.jpg'
        with open(filename, 'wb') as f:
            f.write(pic)
        self.m_bitmap2.SetBitmap(wx.Image(filename, wx.BITMAP_TYPE_ANY).ConvertToBitmap())

    def luk( self, event ):
        self.Hide()

class krypter_billede(gui.PicFrame):
    def __init__(self, parent):
        gui.PicFrame.__init__(self, parent)

    def load_pic( self, event ):
        pic = wx.Image(self.m_filePicker1.GetPath(), wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.m_bitmap1.SetBitmap(pic)

    def krypter_billede( self, event ):
        filename = 'kryptpic'
        outfile = open(filename, 'wb')
        with open(self.m_filePicker1.GetPath(), "rb") as imageFile:
            str = base64.b64encode(imageFile.read())
            print(str) # bare test
            print(int.from_bytes(str, "big")) # bare test
            # Her skal billedet krypteres, og så skal det dumpes
            pickle.dump(int.from_bytes(str, "big"), outfile)
            outfile.close()
        self.Hide()

class mainFrame(gui.MainFrame):
    def __init__(self, parent):
        gui.MainFrame.__init__(self, parent)
        self.krypter_billede = krypter_billede(self)
        self.dekrypter = dekrypt(self)

    def afslut( self, event ):
        exit(0)

    def vaelg_billede( self, event ):
        self.krypter_billede.Show()

    def vaelg_billede_dekrypt( self, event ):
        self.dekrypter.Show()


app = wx.App(False)
frame = mainFrame(None)
frame.Show(True)
app.MainLoop()